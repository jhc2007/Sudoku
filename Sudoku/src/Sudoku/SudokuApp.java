package Sudoku;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SudokuApp {

    public static void main(String[] args) throws FileNotFoundException {
        Sudoku jan;
        if(args.length == 1) {
            FileInputStream f = new FileInputStream(args[0]);
            Scanner s = new Scanner(f);
            int n, i;
            String str;
            Integer[] data = new Integer[81];
            i=0;
            while(s.hasNext()) {
                n = 0;
                str = s.next();
                while(n < str.length() && (str.charAt(n) < '0' || str.charAt(n) > '9'))
                    n++;
                if(i < 81 && n < str.length()) {
                    data[i] = str.charAt(n) - '0';
                    i++;
                }
            }
            String[] new_args = new String[i];
            for(n=0; n<i; n++)
                new_args[n] = String.valueOf(data[n]);
            jan = new Sudoku("Sudoku", new_args);
        }
        else
            jan = new Sudoku("Sudoku", args);
        jan.init();
    }
}
