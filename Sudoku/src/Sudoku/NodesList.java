/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sudoku;

/**
 *
 * @author jhc
 */
public class NodesList {
   	Node first;
	
	NodesList() {

		first = null;

	}

	void addNode(int i, int j) {

		if(first == null)

			first = new Node(i, j);

		else {

			Node tmp = new Node(i, j);
			tmp.next = first;
			first.prev = tmp;
			first = tmp;

		}

	}
 
}
