package Sudoku;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JTextField;

public class Sudoku extends Frame implements KeyListener, ActionListener, WindowListener {

    MyTextField mytf[][];
    String data[];
    Label lab;
    
    public Sudoku(String name, String arg[]) {
	super(name);
        data = arg;
        lab = new Label();
    }
    
    public void init() {
    	int i, j, k, m, n;

    	mytf = new MyTextField[9][9];
    	setSize(300, 400);
    	setLayout(new BorderLayout());
    	Panel pan = new Panel();
    	pan.setLayout(new GridLayout(3, 3, 1, 1));
        pan.setBackground(Color.BLUE);
    	for(m=0; m<3; m++) {
	    for(i=0; i<3; i++) {
		Panel pan2 = new Panel();
		pan2.setLayout(new GridLayout(3, 3));
                pan2.setBackground(Color.BLUE);
		for(j=3*m; j<3*m+3; j++) {
		    for(k=3*i; k<3*i+3; k++) {
                                                
                        mytf[j][k] = new MyTextField("", 1, j, k);
                        mytf[j][k].setBackground(Color.white);
                        mytf[j][k].addKeyListener(this);
			mytf[j][k].setFont(new Font("MONOSPACED", Font.BOLD, 22));
                        mytf[j][k].setHorizontalAlignment(JTextField.CENTER);
                        pan2.add(mytf[j][k]);
		    }
		}
		pan.add(pan2);    
	    }
    	}

        n=0;
        i=0;
        j=0;
        while(n < data.length) {
            if(!data[n].equals("0")) {
                        mytf[i][j].setText(data[n]);
                        mytf[i][j].setBackground(Color.LIGHT_GRAY);
            }
            j++;
            n++;
            if(j == 9) {
                j=0;
                i++;
            }
            
        }
        
    	add(pan, BorderLayout.CENTER);
        
        lab.setFont(new Font("MONOSPACED", Font.BOLD, 24));
        lab.setText("Sudoku");
        lab.setBackground(Color.LIGHT_GRAY);
        add(lab, BorderLayout.NORTH);
             
    	Font fnt = new Font("SANS_SERIF", Font.TRUETYPE_FONT, 16);
	Panel pan3 = new Panel();
        pan3.setBackground(Color.BLUE);
    	
	Button res = new Button("Reset");
	Button sol = new Button("Solve");
	Button clo = new Button("Close");
              
        res.setFont(fnt);
	sol.setFont(fnt);
	clo.setFont(fnt);

    	res.addActionListener(this);
	clo.addActionListener(this);
	sol.addActionListener(new java.awt.event.ActionListener() { 
            
            @Override
                public void actionPerformed(java.awt.event.ActionEvent e) {
                lab.setText("Wait a moment...");
		int i, j;
                Boolean solution = true;
		NodesList list = new NodesList();
		for(i=0; i<9; i++)
               		for(j=0; j<9; j++) {

				if(mytf[i][j].getText().equals("")) {
					list.addNode(i, j);
                                        mytf[i][j].setVisible(false);
                                }

			}
		
		Node node = list.first;
	
        	while(node != null) {

			node.val++;
			while(node.val<10 && !verifica(node.val, node.lin, node.col))
                		node.val++;
			if(node.val == 10) {
                		node.val = 0;
                		node = node.prev;
                                if(node != null)
                                    mytf[node.lin][node.col].setText("");
                                else
                                    solution = false;
            		}
            		else
                		node = node.next;
		}
                node = list.first;
                while(node != null) {
                    mytf[node.lin][node.col].setVisible(true);
                    if(solution)
                        mytf[node.lin][node.col].setBackground(Color.yellow);
                    node = node.next;
                }
                
                if(!solution) {
                    lab.setText("NO SOLUTION");
                    lab.setBackground(Color.RED);  
                    
                    
                }
                else {
                    lab.setText("SOLUTION");
                    lab.setBackground(Color.YELLOW); 
                }
                }});
	
        
        sol.setPreferredSize(new Dimension(150, 30));
        res.setPreferredSize(new Dimension(60, 30));
        clo.setPreferredSize(new Dimension(60, 30));
        
        
	pan3.add(sol);
        pan3.add(res);
        pan3.add(clo);

    	add(pan3, BorderLayout.SOUTH);

	addWindowListener(this);
    	setVisible(true);
    } 

    @Override
    public void windowClosing(WindowEvent e) {
	System.exit(0);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
    	int i, j, n=0;
	if((((Button) e.getSource()).getLabel()).equals("Close"))
		System.exit(0);
        lab.setText("Sudoku");
        lab.setBackground(Color.LIGHT_GRAY);
       
        for(i=0; i<9; i++)
            for(j=0; j<9; j++) {
                    if(n >= data.length || data[n].equals("0")) {
                        mytf[i][j].setText("");
                        mytf[i][j].setBackground(Color.white);
                    }
                    else {
                        mytf[i][j].setText(data[n]);
                        mytf[i][j].setBackground(Color.LIGHT_GRAY);
                    }
                    n++;
                    }
            
    }
    
    @Override
    public void keyPressed(KeyEvent k)
    {
        limpaCor();
    }
    
    @Override
    public void keyTyped(KeyEvent k)  
    {	
	String str;
	int lin = ((MyTextField)k.getSource()).lin;
	int col = ((MyTextField)k.getSource()).col;
	limpaCor();
	if(!mytf[lin][col].getText().equals("")) {
	    if(k.getKeyChar() == 8)
		mytf[lin][col].setText("");
	    else
		k.consume();
	    return;
	}
    	if(k.getKeyChar() < '1' || k.getKeyChar() > '9')
	    k.consume();
	else {
	    str = "" + k.getKeyChar();
	    k.consume();
	    mytf[lin][col].setText(str);
	    if(!verifica(0, lin, col))
		mytf[lin][col].setText("");
            else
                //mytf[lin][col].setText("");
                mytf[lin][col].setBackground(Color.LIGHT_GRAY);
    	}
    }
    
    @Override
    public void keyReleased(KeyEvent k) 
    {  
    }
        
    public Boolean verificaLin(int val, int lin, int col) {
	int i;
	if(val == 0) {
		for(i=0; i<9; i++) {
	    		if(i != col)
	    			if(mytf[lin][i].getText().equals(mytf[lin][col].getText())) {
		    			mytf[lin][i].setBackground(Color.red);
		    			return false;
	    			}
		}
	}
	else {
		for(i=0; i<9; i++)
            		if(mytf[lin][i].getText().equals(Integer.toString(val)))
                		return false;
	}
	return true;
    }
    
    public Boolean verificaCol(int val, int lin, int col) {
	int i;
	if(val == 0) {
	for(i=0; i<9; i++) {
	    if(i != lin)
	    	if(mytf[i][col].getText().equals(mytf[lin][col].getText())) {
		    mytf[i][col].setBackground(Color.red);
		    return false;
	    	}
	}
	}
	else {
		for(i=0; i<9; i++)
            		if(mytf[i][col].getText().equals(Integer.toString(val)))
				return false;
	}
	return true;
    }
    
    public Boolean verificaReg(int val, int lin, int col) {
	
	int i, j, l, c;
	l = lin - lin % 3;
        c = col - col % 3;
	if(val == 0) {
	for(i=l; i<l+3; i++)
            for(j=c; j<c+3; j++)
		if(i != lin || j != col)
		    if(mytf[i][j].getText().equals(mytf[lin][col].getText())) {
		    	mytf[i][j].setBackground(Color.red);
			return false;
		    }
	}
	else {
	for(i=l; i<l+3; i++)
            for(j=c; j<c+3; j++)	
		if(mytf[i][j].getText().equals(Integer.toString(val)))
                    return false;
	}
	return true;
    }
	

    public Boolean verifica(int val, int lin, int col) {
        if(val == 0) {
            Boolean flag = true;
            if(!verificaLin(val, lin, col))
                flag = false;
            if(!verificaCol(val, lin, col))
                flag = false;
            if(!verificaReg(val, lin, col))
                flag = false;
            return flag;
        }
        if(verificaLin(val, lin, col) && verificaCol(val, lin, col) && verificaReg(val, lin, col)) {
            mytf[lin][col].setText(Integer.toString(val));
            return true;                    
        }
        return false;
    }


    public void limpaCor() {
	int i, j;
	for(i=0; i<9; i++)
	    for(j=0; j<9; j++)
		if(!mytf[i][j].getText().equals(""))
                    mytf[i][j].setBackground(Color.LIGHT_GRAY);
            
    }

    @Override
    public void windowOpened(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowClosed(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowIconified(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowActivated(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }
}

