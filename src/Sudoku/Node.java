/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sudoku;

/**
 *
 * @author jhc
 */
public class Node {

    int lin;
	int col;
	int val;
	Node next;
	Node prev;

	Node(int i, int j) {

		lin=i;
		col=j;
		val=0;
		next=null;
		prev=null;		

	}
	
	void setLinCol(int i, int j) {

		lin=i;
		col=j;

	}

}
